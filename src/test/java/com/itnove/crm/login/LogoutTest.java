package com.itnove.crm.login;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.DashboardPage;
import com.itnove.crm.pages.LoginPage;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class LogoutTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.logout(driver,wait,hover);
        assertTrue(loginPage.isLoginButtonPresent(driver,wait));
    }
}
