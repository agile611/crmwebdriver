package com.itnove.crm.login;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.LoginPage;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class LoginIncorrecteTest extends BaseTest {

    public void checkErrors(String user, String passwd){
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya incorrecte.
        // Es clicka  al botó de login. 
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(user, passwd);
        //Comprovar l'error
        assertTrue(loginPage.isErrorMessagePresent(driver, wait));
        assertEquals(loginPage.errorMessageDisplayed(),
                "You must specify a valid username and password.");
    }

    @Test
    public void testApp() throws InterruptedException {
        //User ok passw KO
        checkErrors("user","nami");
        //User KO passwd OK
        checkErrors("resu","bitnami");
        //User KO passwd KO
        checkErrors("resu","nami");
    }
}
