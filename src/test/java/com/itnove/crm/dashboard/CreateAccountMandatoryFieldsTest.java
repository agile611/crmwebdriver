package com.itnove.crm.dashboard;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.CreateAccountPage;
import com.itnove.crm.pages.DashboardPage;
import com.itnove.crm.pages.EditAccountPage;
import com.itnove.crm.pages.LoginPage;
import org.junit.Test;

import java.util.UUID;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class CreateAccountMandatoryFieldsTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        String name = UUID.randomUUID().toString();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.createButtonClick(hover);
        dashboardPage.clickOnCreateDocumentLink(hover);
        CreateAccountPage createAccountPage = new CreateAccountPage(driver);
        createAccountPage.fillName(name);
        createAccountPage.saveAccount();
        EditAccountPage editAccountPage = new EditAccountPage(driver);
        assertEquals(name.toUpperCase(),editAccountPage.getTitulo().toUpperCase());
     }
}
