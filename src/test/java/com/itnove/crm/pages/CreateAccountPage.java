package com.itnove.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class CreateAccountPage {

    private WebDriver driver;

    @FindBy(id = "name")
    public WebElement nameTextbox;

    @FindBy(id = "SAVE")
    public WebElement saveButton;

    public void fillName(String name){
        nameTextbox.sendKeys(name);
    }

    public void saveAccount(){
        saveButton.click();
    }

    public CreateAccountPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
