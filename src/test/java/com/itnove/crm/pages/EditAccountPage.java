package com.itnove.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by guillem on 01/03/16.
 */
public class EditAccountPage {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='pagecontent']/div[1]/h2")
    public WebElement titulo;

    public String getTitulo(){
        return titulo.getText();
    }

    public EditAccountPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
